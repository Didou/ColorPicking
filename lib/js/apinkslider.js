function APinkSlider(items) {
	
	var PACK_ITEMS 		= items;
	var DELAY_ANIMATE 	= 600;
	
	var container;
	var thumbnails;
	var overflow;
	
	var slider = {};
	
	var packs = {};
	
	/****************************************************************/
	/*********************         INIT       ***********************/
	/****************************************************************/
	init = function () {
		_initConfig();
		_initPackThumbnails();
		_initSliderNavigation();
		
		setSliderProperty();
		setSliderVisibility();
		
		observeSliderHelper();
	};
	
	_initConfig = function () {
		container 	= $('#thumb-picture');
		thumbnails 	= container.children();
	};
	
	_initSliderNavigation = function () {
		var element  	= $(document.createElement('div')),
			node_nav 	= $(document.createElement('div')),
			nav 		= $(document.createElement('div'));
		
		element.addClass('apinkslider-overflow').attr('id', 'apinkslider-overflow');	
			
		nav.addClass('apinkslider-nav')
			.attr('id', 'apinkslider-nav')
			.prepend('<span class="apinkslider-prev"></span>')
			.append('<span class="apinkslider-next"></span>');
		
		node_nav.addClass('apinkslider-helper')
			.attr('id', 'apinkslider-helper')
			.append(nav);
		
		container.wrapInner(element)
			.append(node_nav);
		
		overflow = $('#apinkslider-overflow');
		slider.navigation = {
			nav : node_nav
		}
	};
	
	_initPackThumbnails = function() {
		var reste = Math.round(parseFloat(thumbnails.length) / PACK_ITEMS);
		var count = 0;
		for(var i = 0 ; i < thumbnails.length; i = i + PACK_ITEMS) {
			thumbnails.slice(i, i + PACK_ITEMS).wrapAll('<div class="apinkslider-pack" id="pack-'+ count +'"></div>');
			count++;
		}
		
		packs.containers = $('div.apinkslider-pack');
	};
	
	_initSliderDots = function () {
		var divDots = $(document.createElement('div'));
		var nav = slider.navigation.nav;
		
		
		divDots.addClass('apinkslider-dots').attr('id', 'apinkslider-dots');
		for(var i = 0 ; i < slider.packs.length ; i++) {
			var dots = $(document.createElement('div'));
			dots.addClass('apinkslider-dot')
				.attr('data-pack', 'pack-' + i);
			
			divDots.append(dots);
		}
		nav.append(divDots);
		
		slider.navigation.dots = divDots;
	};
	
	/****************************************************************/
	/**********************      HELPER      ************************/
	/****************************************************************/
	
	_getPropertyPackItems = function () {
		var packs = $('div.apinkslider-pack');
		var firstPack = packs.first(), thumbFirstPack = firstPack.children();
		
		var packWidth = 0, totalWidth = 0, count;
		
		thumbFirstPack.each(function () {
			packWidth += parseInt($(this).width());
		});
		
		totalWidth = packWidth * packs.length;
	
		slider.packWidth 		= packWidth;
		slider.maxWidth 		= totalWidth;
		slider.packs 			= packs;
		slider.nativeOffset  	= {
			left : firstPack.offset().left,
			top : firstPack.offset().top
		};
		
		return true;
	};
	
	/****************************************************************/
	/*********************       SLIDER      ************************/
	/****************************************************************/
	setSliderProperty = function () {
		if(_getPropertyPackItems()) {
			container.css({
				width : slider.packWidth + 'px'
			});
			
			overflow.css({
				width : slider.maxWidth + 'px'
			});
		}	
	};
	
	setSliderVisibility = function () {
		var packs = slider.packs;
		var count = 0;
		
		packs.each(function () {
			var $this = $(this);
			if(count < 1) {
				$this.removeClass('no-select').addClass('apinkslider-show');
				count++;
				
				return true;
			}
			$this.addClass('apinkslider-hide no-select');
			
		});
		
		_initSliderDots();
	};
	
	slide = function (e) {
		var $dot = $(e.target),context = $dot.attr('data-pack'), position = context.replace( /^\D+/g, ''),
			parent = overflow, prevPack = parent.find('.apinkslider-show');
		
		
		var pack = $('#' + context);

		if(pack.hasClass('apinkslider-show')) {
			return true;	
		}
		
		var left = Math.round((parseInt(position) * parseInt(slider.packWidth)) * -1);		
		
		parent.css({
			transform : 'translateX('+ left +'px)'
		});
		
		setTimeout(function () {
			prevPack.removeClass('apinkslider-show').addClass('apinkslider-hide no-select');
			pack.addClass('apinkslider-show').removeClass('apinkslider-hide no-select');
		}, 100);
	};
	
	slideNext = function (e) {
		var currentSlide = overflow.find('.apinkslider-show'), context = currentSlide.attr('id'),
			position = context.replace( /^\D+/g, '');
		
		var parent = overflow,
			nextSlide = currentSlide.next();
	
		var left = Math.round(((parseInt(position) * parseInt(slider.packWidth)) + parseInt(slider.packWidth)) * -1);		
	
		if(parseInt(left * -1) >= parseInt(slider.maxWidth)) {
			return true;
		}
		
		parent.css({
			transform : 'translateX('+ left +'px)'
		});
		
		setTimeout(function () {
			currentSlide.removeClass('apinkslider-show').addClass('apinkslider-hide no-select');
			nextSlide.addClass('apinkslider-show').removeClass('apinkslider-hide no-select');
		}, 100);
	};
	
	slidePrev = function (e) {
		var currentSlide = overflow.find('.apinkslider-show'), context = currentSlide.attr('id'),
			position = context.replace( /^\D+/g, '');
		
		var parent = overflow,
			prevSlide = currentSlide.prev();
		
		if (parseInt(position) == 0) {
			return true;
		}
	
		var left = Math.round((parseInt(position) * parseInt(slider.packWidth)) - parseInt(slider.packWidth)) * -1 ;		
	
		parent.css({
			transform : 'translateX('+ left +'px)'
		});
		
		setTimeout(function () {
			currentSlide.removeClass('apinkslider-show').addClass('apinkslider-hide no-select');
			prevSlide.addClass('apinkslider-show').removeClass('apinkslider-hide no-select');
		}, 100);
	};
	/****************************************************************/
	/*********************      Observer      ***********************/
	/****************************************************************/
	observeSliderHelper = function () {
		$(document).on('click', '.apinkslider-dot', slide);
		$(document).on('click', '.apinkslider-next', slideNext);
		$(document).on('click', '.apinkslider-prev', slidePrev);
	};
	
	
	init();
}