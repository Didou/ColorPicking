var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
	uglify			= require('gulp-uglify'),
	concat			= require('gulp-concat'),
	cleanCSS		= require('gulp-clean-css'),
    autoprefixer    = require('gulp-autoprefixer');

gulp.task('bomi', function () {
    // Task for CSS
    gulp.src('./assets/sass/**/*.scss')
        .pipe(autoprefixer())
        .pipe(sass({
            outputStyle : 'compact'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('eunji', function() {
    gulp.watch('assets/sass/**/*.scss', ['bomi']);
});

gulp.task('naeun', function () {
	gulp.src('./assets/js/*.js')
		.pipe(uglify())
		.pipe(gulp.dest('public/production/js'))
});

gulp.task('hayoung', function () {
	return gulp.src('./public/css/*.css')
		.pipe(cleanCSS())
		.pipe(concat('style.min.css'))
		.pipe(gulp.dest('public/production'));
});