$(document).ready(function () {
    
	var $body 					= $('body');
	
    var COLOR_ID                = 'color',
        IS_DOWN                 = false;
    
    /* 
     * Class for cursor
     */
    var CLASS_CURSOR_DEFAULT    = 'cursor',
        CLASS_CURSOR_HOVER      = 'cursor on-grab',
		windowWidth 			= $(window).width(),
		marginLeft 				= 0;   
	
    /* 
     * Class for SVG form
     */
    var CLASS_FORM_DEFAULT      = 'formTest',
        CLASS_FORM_FOCUS        = 'formTest is-focus',
        CLASS_FORM_FINAL        = 'formTest is-color';
    
    /*
     * Default varhisiables SVG 
     */
    var DEFAULT_STROKE_WIDTH    = '2',
        DEFAULT_STROKE_COLOR    = '#000000',
        HOVER_STROKE_WIDTH      = '5',
        HOVER_STROKE_COLOR      = '#000000',
        
        currentColor            = "",
        target                  = "",
        oldColor                = "";
    
    
    var form                    = $('#formSVG'),
        path                    = $('path'),
        formTest                = $('path.formTest'),
        
        btnColor                = $('.color'),
    
        // Cursor
        cursor                  = $('#goutte'),
        svgForm                 = $('path.cursor'),
        $body                   = $('body');
    
    var $input                  = $('#hiddenColor');
    var countDifficulty         = $('#countDifficult');
    
    /*
     * Init difficulty
     */ 
    var _DIFFICULTY             = ['débutant', 'novice', 'boss'];
    countDifficulty.val(0);
    
    /*
     * Historique 
     */
    var historique              = $('#historique'),
        histo_items             = historique.find('.histo-item'),
        
        DEFAULT_HISTO_CLASS     = 'histo-item',
        GRAB_HISTO_CLASS        = 'is-grab',
        SWIPE_HISTO_CLASS       = 'is-swipe';
    
    
    /*
     * Export
     */
    var $btnExport              = $('#btnExport');
    
    
    // ------------------------------------------------- \\
    // --------- Désactive la sélection de texte ------- \\
    // ------------------------------------------------- \\
    disableSelection(document.body);
    
    // ------------------------------------------------- \\
    // -------- Init difficulté sur les couleurs ------- \\
    // ------------------------------------------------- \\
    var $classes = getSvgInformation(form, _DIFFICULTY);
    
    
    
    // ------------------------------------------------- \\
    // --------- Evènement souris pour la goutte ------- \\
    // ------------------------------------------------- \\
    $(document).on({
        mousedown : function(e) {
			if (e.target.id == COLOR_ID) {
                var color   = $(e.target),
                    pos     = getPosition(e);
                
                currentColor = color.attr('data-type');
                svgForm.attr('class', CLASS_CURSOR_HOVER).css('fill', currentColor);
                
                $body.addClass('on-select');
                
				posX = parseInt(pos.posX) - marginLeft;
				
                cursor.css("top", pos.posY).css("left", posX).addClass('on-grab');
                
                // Set les variables de check
                target = e.target.id;
                IS_DOWN = true;
            } else {
                return true;
            }
        },
        mousemove : function(e) {
            if (checkTarget(target)) {
                var pos = getPosition(e);
                
				posX = parseInt(pos.posX) - marginLeft;
				
                if (svgForm.attr('class') == CLASS_CURSOR_HOVER) {
                    cursor.css("top", pos.posY).css("left", posX);
                }
            }  
        },
        mouseup : function(e) {
            if (checkTarget(target)) {
                $body.removeClass('on-select');
                cursor.removeClass('on-grab');
                
                svgForm.attr('class', CLASS_CURSOR_DEFAULT);
            
                // Reset valeurs
                currentColor = "";
                target = "";
                IS_DOWN = false;
            }
        }
    }); 
    
    
    // ------------------------------------------------- \\
    // ----------- Evènement souris sur le SVG --------- \\
    // ------------------------------------------------- \\
	$(document).on('mouseenter', '.formTest', function (e) {
		if (IS_DOWN) {
			var $this               = $(this),
				$parent             = $this.parent(),
				oldColor            = rgb2hex($this.css('fill')),
				rgb                 = hexToRgb(currentColor),
				HOVER_STROKE_COLOR  = lightenDarkenColor(currentColor, 20),
				style               = "rgba("+ rgb.r +", "+ rgb.g +", "+ rgb.b +", 0.5)";

			if($this.css('fill') == 'rgba(0, 0, 0, 0)'){
				oldColor = 'transparent';
			}

			$input.val(oldColor);
			$this.attr('class', CLASS_FORM_FOCUS).css({
				'fill'          : style,
				'stroke-width'  : HOVER_STROKE_WIDTH,
				'stroke'        : HOVER_STROKE_COLOR
			});
		}
	});
	
	$(document).on('mouseleave', '.formTest', function (e) {
		if (IS_DOWN) {
			var $this       = $(this),
				$parent     = $this.parent(),
				$oldColor   = $input.val();

			$this.attr('class', CLASS_FORM_DEFAULT).css({
				'fill'          : $oldColor,
				'stroke-width'  : DEFAULT_STROKE_WIDTH,
				'stroke'        : DEFAULT_STROKE_COLOR
			});
		}
	});
	
	
	$(document).on('mouseup', '.formTest', function (e) {
		if (currentColor.length > 1) {
			var $this   = $(this),
				style   = currentColor;

			$this.attr('class', CLASS_FORM_FINAL).css({
				'fill'          : style,
				'stroke-width'  : DEFAULT_STROKE_WIDTH,
				'stroke'        : DEFAULT_STROKE_COLOR
			});
		}
	});    
});


/*
 * Observe les évènements lié au coloriage 
 *
 */
function observeSVG(svg, difficuly, input){
    var $path = $('path.formTest');
    
    var $el = parseInt(input.val());
    $path.each(function(){
        var $this = $(this);
        if($this.attr('class') == 'formTest is-color'){
            $el++;
            $('#countDifficult').val($el);
        }
    });
    
    var currentStep = $('#countDifficult').val();
    
    switch (true) {
        case currentStep < parseInt(difficuly[0].key) :
            console.log(difficuly[0].label);
            $('#level').html(difficuly[0].label);
            break;
        case currentStep < parseInt(difficuly[1].key) :
            console.log(difficuly[1].label);
            $('#level').html(difficuly[1].label);
            break;
        case currentStep < parseInt(difficuly[2].key) :
            console.log(difficuly[2].label);
            $('#level').html(difficuly[2].label);
            break;    
    }
}


/*
 *
 *
 */
function getSvgInformation(svg, difficulty){
    var $class      = svg.find('path'),
        $count      = $class.length,
        $nextStep   = 0,
        $key        = "level-",
        $res        = [];
    
    for(var i = 0 ; i < $count ; i++){
        if($count > 1){
            var key = $key + i
            var index = Math.round($class.length / 3);
            
            $nextStep = $nextStep + index;
            
            var level = {
                key     : $nextStep, 
                label   : difficulty[i]
            }
            
            $res.push(level);       
            $count = $count - index;
        }
    }
    return $res.length > 1 ? $res : false;
}

/*
 * Assombrie une couleur passé en paramètre
 *
 * @return : string
 */
function lightenDarkenColor(col, amt){
    var usePound = false;
  
    if (col[0] == "#") {
        col = col.slice(1);
        usePound = true;
    }
 
    var num = parseInt(col,16);
 
    var r = (num >> 16) + amt;
 
    if (r > 255) r = 255;
    else if  (r < 0) r = 0;
 
    var b = ((num >> 8) & 0x00FF) + amt;
 
    if (b > 255) b = 255;
    else if  (b < 0) b = 0;
 
    var g = (num & 0x0000FF) + amt;
 
    if (g > 255) g = 255;
    else if (g < 0) g = 0;
 
    return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
}

/*
 * Récupère les positions X et Y de la souris 
 *
 * @return : array()
 */ 
function getPosition(e) {
    return response = {
        posX : e.pageX,
        posY : e.pageY
    };
};

/*
 * Check si on a une couleur de sélectionner
 *
 * @return : boolean
 */ 
function checkTarget(target) {
    if(typeof target != "") {
        return true;
    }
    return false;
};

/*
 * Convert hexa code to rgba
 *
 * @return : array()
 */ 
function hexToRgb(hex, alpha) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    var toString = function () {
        if (this.alpha == undefined) {
            return "rgb(" + this.r + ", " + this.g + ", " + this.b + ")";
        }
        if (this.alpha > 1) {
            this.alpha = 1;
        } else if (this.alpha < 0) {
            this.alpha = 0;
        }
        return "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.alpha + ")";
    } 
    if (alpha == undefined) {
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16),
            toString: toString
        } : null;
    }
    if (alpha > 1) {
        alpha = 1;
    } else if (alpha < 0) {
        alpha = 0;
    }
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
        alpha: alpha,
        toString: toString
    } : null;
}

/*
 * Convert rgba code to hex
 *
 * @return : string
 */ 
function rgb2hex(rgb){
    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
    return (rgb && rgb.length === 4) ? "#" +
    ("0" + parseInt(rgb[1],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[2],10).toString(16)).slice(-2) +
    ("0" + parseInt(rgb[3],10).toString(16)).slice(-2) : '';
}

/*
 * Empêche la sélection du texte dans la page
 */ 
function disableSelection(target) {
    if (typeof target.onselectstart!="undefined")
        target.onselectstart=function(){return false}
    else if (typeof target.style.MozUserSelect!="undefined")
        target.style.MozUserSelect="none"
    else
        target.onmousedown=function(){return false}
    target.style.cursor = "default"
}