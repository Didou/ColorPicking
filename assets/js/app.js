$(document).ready(function(e){
	var $_observer = [{ label : 'index', step : 'init' }, { label : 'sexe', step : 'init' }, { label : 'form', step : 'init' }, { label : 'picking', step : 'picking' }, { label : 'choose', step : 'choose' }, { label : 'rendering', step : 'rendering' }, { label : 'final', step : 'final' } ];
	
	// ----------------------------------------------------------- \\
	// ----------------------- VARIABLES ------------------------- \\
	// ----------------------------------------------------------- \\
	var $body 				= $('body'),
		$bgColor			= $('#bgColor'),
		$_index 			= $('#obs-index');
		
	// Formulaire
	var VALIDATION_PASSED 	= 'validation-passed',
		VALIDATION_FAILED 	= 'validation-error';
	
	// Picking 
	var $btnHistory			= $('#button-history');
	
	// User
	var user 				= new User(null, null, null);
				
	// Popin 
	var popin 				= $('#popin');
	
	// ----------------------------------------------------------- \\
	// ------------------ Load Partial Event --------------------- \\
	// ----------------------------------------------------------- \\
	
	/***************/
	/* Step  index */
	/***************/ 
	$_index.on('click', function (e) {
		var string = $(this).attr('data-obs');
		
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i + 1];
				load(string, next);
				return false;
			}	
		}		
		return false;
	});
	
	/***************/
	/** Step Sexe **/
	/***************/
	$body.on('submit', '#obs-sexe', function (e) {
		var string = $(this).attr('data-obs');
		
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i + 1];
				var $_form = $('body').find('form');
				
				if(validFormSexe($_form, VALIDATION_PASSED, VALIDATION_FAILED, 1)){
					// Init User - Set data["sexe"]
					var sexe = $_form.find('div.validation-passed > input[type="text"]');
					user.setSexe(sexe.attr('id'));
					localStorage.setItem("user", JSON.stringify(user));
					
					load(string, next);
					return false;
				}
			}	
		}
		return false;
	});
	
	/***************/
	/** Step Form **/
	/***************/
	$body.on('submit', '#obs-form', function (e) {
		var string = $(this).attr('data-obs');
		
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i + 1];
				var $_form = $('body').find('form');
				
				if(validForm($_form, VALIDATION_PASSED, VALIDATION_FAILED, 2)){
					
					// Load User - Set data["name"] and data["classe"]
					user.setName($_form.find('input[name="name"]').val());
					user.setClasse($_form.find('input[name="class"]').val());
					
					localStorage.setItem("user", JSON.stringify(user));
					
					load(string, next);
					return false;
				}
			}	
		}
		return false;
	});
	
	/****************/
	/* Step Picking */
	/****************/
	$body.on('click', '#stepPicking', function (e) {
		var string = $(this).attr('data-obs');
		
		if(!checkTestRVB()) {
			loadPopin();
			return true;	
		}
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i + 1];
				load(string, next);
				return false;
			}	
		}
		return false;
	});
	
	$body.on('click', '#stepChoose', function (e) {
		var string = $(this).attr('data-obs');
		
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i + 1];
				load(string, next);
				return false;
			}	
		}
		return false;
	});
	
	/******************/
	/* Step Rendering */
	/******************/
	$body.on('click', '#stepRendering', function (e) {
		var string = $(this).attr('data-obs');
		
		for(var i = 0 ; i < $_observer.length ; i++){
			if($_observer[i].label == string){
				var next = $_observer[i];
				load(string, next);
				return false;
			}	
		}
		return false;
	});

	
	// ----------------------------------------------------------- \\
	// ------------------- PREVENT CLOSE ------------------------- \\
	// ----------------------------------------------------------- \\
//	$(window).bind('beforeunload', function(){
//		var $btn = popin.find('button.btn');
//		
//		popin.addClass('is-active');
//		$bgColor.addClass('is-active');
//		
//		$btn.on('click', function (e) {
//			var $this = $(this);
//			
//			if ($this.hasClass('btn-yes')) {
//				location.reload();
//			}
//			
//			if ($this.hasClass('btn-no')) {
//				return false;
//			}
//		});
//		
//		return '';
//	});
	
	// ----------------------------------------------------------- \\
	// --------------------- FORM SEXE --------------------------- \\
	// ----------------------------------------------------------- \\
	var isProcess = false;
	
	$body.on('mouseenter', '.iptSexe', function (e) {
		var $this 	= $(this),
			$parent = $this.parent();
	
		var $items	= $parent.find('div.item');
		var i = 0;
		var max = $items.length;
		
		$items.removeClass('hidden');
			
	});
	
	$body.on('mouseleave', '.iptSexe', function (e) {
		var $this 	= $(this),
			$parent = $this.parent();
		
		var $items	= $parent.find('div.item');
		
		$items.addClass('hidden');
	});
	
	$body.on('click', '.iptSexe', function (e) {
		var $this 	= $(this),
			$parent = $this.parent();
		
		$parent.removeClass(VALIDATION_FAILED).addClass(VALIDATION_PASSED);
		$body.off('mouseenter', '.iptSexe');
		$body.off('mouseleave', '.iptSexe');
	});
	
	
	
	
	// ----------------------------------------------------------- \\
	// --------------------- FOMULAIRE --------------------------- \\
	// ----------------------------------------------------------- \\
	$body.on('input', '.iptText', function(e) {
		var $this 	= $(this),
			$parent = $this.parent(),
			$obj  	= '<div class="validation"></div>';
		
		if($this.attr('id') == 'pseudo'){
			return $this.val().length >= 4 ? setValidation($this, VALIDATION_PASSED, VALIDATION_PASSED, VALIDATION_FAILED) : setValidation($this, VALIDATION_FAILED, VALIDATION_PASSED, VALIDATION_FAILED);
		}
		
		if($this.attr('id') == 'classe'){
			return $this.val().length >= 1 ? setValidation($this, VALIDATION_PASSED, VALIDATION_PASSED, VALIDATION_FAILED) : setValidation($this, VALIDATION_FAILED, VALIDATION_PASSED, VALIDATION_FAILED);
		}
	});
	
	
	// ----------------------------------------------------------- \\
	// ---------------------- PICKING ---------------------------- \\
	// ----------------------------------------------------------- \\
	
	// OwlCarousel
	$body.on('click', 'a.miniatures', function(e){
		var $this 	= $(this),
			$svg 	= $this.children().clone();
		
		// Update color_wrapping 
		var $wrapper    = $('#picking-perso'),
			$children   = $wrapper.children();
		
		if($children.length > 0){
			$wrapper.empty();
		}
		
		$wrapper.append($svg);
	});
});




/*
 * Load partial in index.html
 * 
 * @params index : string
 * @params next : string
 * @params url : string
 *
 * @return : object(html)
 */
function load(index, next){
	var main 			= $('#main'),
		$content 		= main.find('.wrapper'); 	

	var BASE_URL 		= location.href,
		ROOT_PARTIALS 	= '/partials/',
		PARTIALS_EXTEND = '.html',
		
		CLASS_INIT		= 'wrapper',
		CLASS_AFTER     = 'apink-boy',
		CLASS_RESIZE	= 'is-resize',
		CLASS_EXPAND	= 'is-expand';

	index = BASE_URL.lastIndexOf("/");
	BASE_URL = BASE_URL.substring(0, index);

	var href = BASE_URL + ROOT_PARTIALS + next.label + PARTIALS_EXTEND;
	$content.removeClass(CLASS_EXPAND).addClass(CLASS_RESIZE).empty();

	if(next.step != 'init'){
		$content.addClass(CLASS_AFTER);
		main.addClass(CLASS_AFTER);
	} else {
		$content.removeClass(CLASS_AFTER);
		main.removeClass(CLASS_AFTER);
	}
	
	setTimeout(function(){
		// Remove main HTML
		$.ajax({
			url 	: href,
			method 	: 'GET',
			success : function(data){
				$content.removeClass(CLASS_RESIZE).addClass(CLASS_EXPAND);
				
				setTimeout(function(){
					$content.html(data);
					var $div = $content.children();
					$div.animate({
						opacity : 1
					}, 500);
				}, 1000);
				
				return false;
			},
		});	
	}, 600);
}

/*
 * Set class for input validation
 * 
 * @params input : string
 * @params validation : string
 * @params validation_success : const
 * @params validation_error : const
 *
 * @return : null
 */
function setValidation(input, validation, validation_success, validation_error){
	var parent = input.parent(),
		$validation = parent.find('div.validation-wrapper');
	$validation.addClass('is-active');
	if(validation == validation_success){
		parent.removeClass(validation_error).addClass(validation_success);	
		$validation.removeClass(validation_error).addClass(validation_success);
	} else {
		parent.removeClass(validation_success).addClass(validation_error);	
		$validation.removeClass(validation_success).addClass(validation_error);
	}
}

/*
 * Set class for input validation
 * 
 * @params object : form
 * @params validation_success : const
 * @params validation_error : const
 *
 * @return : bool
 */
function validForm (form, validation_success, validation_error, field) {
	var $field = form.find('div.line');
	
	var $_errors = [];
	var count = 0;
	
	$field.each(function(){
		var $this 	= $(this),
			$input 	= $this.find('input[type="text"]');
		
		if($this.hasClass(validation_success)){
			$input.removeClass(validation_error).addClass(validation_success);
			count++;
			
		} else {
			$input.removeClass(validation_success).addClass(validation_error);
		}
	});

	return count == parseInt(field) ? true : false;
}

/*
 *. Set class for input validation
 * 
 * @params object : form
 * @params validation_success : const
 * @params validation_error : const
 *
 * @return : bool
 */
function validFormSexe (form, validation_success, validation_error, field) {
	var $field = form.find('div.input-field');
	
	var $_errors = [];
	var count = 0;
	
	$field.each(function(){
		var $this 	= $(this),
			$input 	= $this.find('input[type="text"]');
		
		if($this.hasClass(validation_success)){
			$input.removeClass(validation_error).addClass(validation_success);
			count++;
			
		} else {
			$input.removeClass(validation_success).addClass(validation_error);
		}
	});

	return count == parseInt(field) ? true : false;
}

/*
 *
 *
 */
function loadPopin () {
	var popin = $('#popin');
	var $btn = popin.find('button.btn');
	var $bgColor = $('#bgColor');
	
	popin.addClass('is-active');
	$bgColor.addClass('is-active');

	$btn.on('click', function (e) {
		var $this = $(this);

		if ($this.hasClass('btn-yes')) {
			popin.removeClass('is-active');	
			$bgColor.removeClass('is-active');	
			
			setTimeout(function () {
				$('.showColorCode').click();
			}, 1000);	
		}

		if ($this.hasClass('btn-no')) {
			popin.removeClass('is-active');
			$bgColor.removeClass('is-active');
		}
		
		if($this.hasClass('btn-validate')) {
			popin.removeClass('is-active');
			$bgColor.removeClass('is-active');
			
			setTimeout(function () {
				oldDataPopin(popin);
			}, 600);
		}
	});
}

function checkTestRVB() {
	var popinTest = $('.wrapperCode');
	var testField = popinTest.find('.find-color');
	var result = true;
	testField.each(function(){
		var $this = $(this);
		if(!$this.hasClass('validation-passed')) {
			result = false;
			return false;
		}
	});
	
	return result;
}

function oldDataPopin (popin) {
	var btnYes 		= popin.find('button.btn-yes');
	var btnNo 		= popin.find('button.btn-no');
	var btnValidate = popin.find('button.btn-validate');
	var span 		= popin.find('span');
	var textNode = "Es-tu prêt à passer le test de validation ?";

	btnYes.css('display', 'inline-block');
	btnNo.css('display', 'inline-block');
	btnValidate.css('display', 'none');
	span.html(textNode);
}

function newDataPopin (popin) {
	var btnYes 		= popin.find('button.btn-yes');
	var btnNo 		= popin.find('button.btn-no');
	var btnValidate = popin.find('button.btn-validate');
	var span 		= popin.find('span');
	var textNode 	= "Image sauvegardée !";

	btnYes.css('display', 'none');
	btnNo.css('display', 'none');
	btnValidate.css('display', 'inline-block');

	span.html(textNode);
}

function successDataPopin (popin) {
	var btnYes 		= popin.find('button.btn-yes');
	var btnNo 		= popin.find('button.btn-no');
	var btnValidate = popin.find('button.btn-validate');
	var span 		= popin.find('span');
	var textNode 	= "Bravo ! Tu as bien travaillé. Prêt(e) pour ta première exposition ?";

	btnYes.css('display', 'none');
	btnNo.css('display', 'none');
	btnValidate.css('display', 'inline-block');

	span.html(textNode);
}