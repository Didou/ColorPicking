$(document).ready(function () {
	
	var $body 			= $('body');
	
	var btnGallery 		= $('button#showGallery');
	var medias = [];
	
	var popin = $('#popin');
	var bgColor = $('#bgColor');
	
	initCanvas();
	
	$body.on('click', '#btnExportSVG', function () {
    	var oSerializer = new XMLSerializer();
		
		var divPicking 	= document.getElementById('picking-perso'), svgNode = divPicking.querySelector('svg');
	
		if(svgNode == null) {
			return true;
		}
		
		var sXML = oSerializer.serializeToString(svgNode);
		var user = JSON.parse(localStorage.getItem('user'));
		
		canvg(document.getElementById('export'), sXML, { ignoreMouse: true, ignoreAnimation : true});
		
		var img = new Image();
		var img64 = document.getElementById('export').toDataURL('image/png');
		img.src = img64;
		
		medias.push(img64);
	
		localStorage.setItem('medias', JSON.stringify(medias));
		
		newDataPopin(popin);
		loadPopin();
	});
	
	btnGallery.on('click', function () {
		var $gallery = $('#gallery');
		if($gallery.length < 1) {
			initGallery();
		}
		
		var $gallery = $('#gallery');
		$gallery.empty();
		
		for(var i = 0 ; i < medias.length; i++) {
			var img = new Image();
			img.height = 750;
			img.width = 750;
			img.src = medias[i];
			
			$gallery.append(img);
		}
	});
});

function initGallery() {
	var body = $('body');
	var divGallery = $(document.createElement('div'));
	divGallery.attr('id', 'gallery');
	
	body.append(divGallery);
}

 
function initCanvas() {
   	var body = $('body');
	var canvas = document.createElement('canvas');
	
	if(document.getElementById('canvas') > 1) {
		return true;
	}
	
	canvas.setAttribute('id', 'export');
	canvas.width 			= 2000;
	canvas.height 			= 2000;
	canvas.style.display 	= 'none';
	
	body.append(canvas);
}